# Teste para Vaga de Frontend

Este repositório foi criado com o objetivo de descrever a tarefa a ser realizada pelo candidato da vaga para programador frontend.

## Sobre o teste

Seu desafio será a criação de uma interface que permita o usuário visualizar uma lista de cursos. Deve permitir também que o usuário consiga selecionar determinado item da lista para visualizar informações mais detalhadas.

Algumas observações:

* A interface deve ser responsiva
* Você pode utilizar  **QUALQUER** préprocessador, framework, taskrunner que desejar.
* Adotar o Material Design como guia de estilo.
* Deverá ser realizado uma requisição HTTP na url https://gitlab.com/fabricio.henriques/test-frontend/raw/master/api/courses.json para recuperar a lista de cursos.

Abaixo segue os protótipos da interface

### Mobile

![](prototypes/mobile.png)

### Desktop

![](prototypes/web.png)

## Avaliação e publicação do teste

Ao finalizar o desenvolvimento, disponibilize o projeto em algum repositório Git (Bitbucket, Github..) e envie o link para o email fabricio.henriques@capgemini.com.

Fique a vontade para entrar em contato nesse email caso ocorra alguma dúvida.

Boa sorte!
